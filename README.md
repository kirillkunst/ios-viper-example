# VIPER architecture example

## Architecture
[VIPER](https://medium.com/yay-its-erica/intro-to-the-viper-design-pattern-swift-3-32e3574dee02)

## Modules

Each module consists of:

- Assembly
- View
	- Implements View Behaviour protocol
- Presenter
	- Implements View handler protocol
- Router
- Contracts
	- View Behaviour protocol (View input)
	- View Handler protocol (View output)


In this example I created 2 modules:

- News List screen
- Detail new item screen

Principal scheme of app architecture:

[![Image](Images/project-structure.png)](Images/project-structure.png)

## Generamba

Each module we can create through [Generamba](https://github.com/rambler-ios/Generamba):

```
generamba gen <module name> <template name>
```

I already created default template for VIPER module:

```
generamba gen <module name> viper-module
```

For example how was created news module for this application:

```
generamba gen News viper-module
```

### Rambafile

To make Generamba work in our project we should have Rambafile in the root of project:

```
### Headers settings
company: ios-viper-example

### Xcode project settings
project_name: ios-viper-example
xcodeproj_path: ios-viper-example.xcodeproj
prefix: PR

### Code generation settings section
# The main project target name
project_target: ios-viper-example

# The file path for new modules
project_file_path: ios-viper-example/Classes/Modules

# The Xcode group path to new modules
project_group_path: ios-viper-example/Classes/Modules

### Dependencies settings section
podfile_path: Podfile

### Templates
templates:
- {name: viper-module}

```

### Template
We should store our template for the project in Templates folder in root folder of the project:

```
Templates/viper-module
```

Each template consists of:

- Rambaspec file
- Files with [Liquid](https://github.com/Shopify/liquid) templates.

Example of Rambaspec file:

```
name: "viper-module"
summary: "Viper Module template"
author: "Kirill Kunst"
version: "1.0"
license: "MIT"

code_files:
- {name: Contracts.swift,  path: Code/contracts.swift.liquid}

- {name: Presenter.swift,  path: Code/presenter.swift.liquid}

- {name: View.swift, path: Code/view.swift.liquid}

- {name: Router.swift, path: Code/router.swift.liquid}

- {name: Assembly.swift, path: Code/assembly.swift.liquid}

```

Example of Liquid template file:

```
//
//  {{ prefix }}{{ module_info.file_name }}
//  {{ module_info.project_name }}
//
//  Created by {{ developer.name }} on {{ date }}.
//  Copyright © {{ year }} {{ developer.company }}. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol {{ module_info.name }}ViewBehavior: class {

}

protocol {{ module_info.name }}EventHandler: class {

}
```

## Tools
### Dependency Manager
[CocoaPods](https://cocoapods.org/)

### Modules building tool
[Generamba](https://github.com/rambler-ios/Generamba)

### Used libraries
- [DITranquillity](https://github.com/ivlevAstef/DITranquillity)
- [RxSwift](https://github.com/ReactiveX/RxSwift)
- [IGListKit](https://github.com/Instagram/IGListKit)

## Maintainers
[Kirill Kunst](https://github.com/leoru)
