//
//  PRPRMainView.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit

// MARK: - View Controller
final class MainViewController: BaseViewController {
    
    var handler: MainEventHandler!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension MainViewController: MainViewBehavior {

}
