//
//  PRMainAssembly.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit
import DITranquillity

final class MainPart: DIPart {
    static func load(container: DIContainer) {
        container.register(MainRouter.init(view:errorHandler:))
            .lifetime(.objectGraph)

        container.register(MainPresenter.init(view:router:))
            .as(MainEventHandler.self)
            .lifetime(.objectGraph)

        container.register {
                MainViewController()
            }
            .as(MainViewBehavior.self)
            .injection(cycle: true, { $0.handler = $1 })
            .lifetime(.weakSingle)
    }
}

final class MainAssembly {
    class func createModule() -> MainViewController {
        return MainAppCoordinator.shared.container.resolve()
    }
}
