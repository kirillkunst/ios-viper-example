//
//  PRMainPresenter.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit

// MARK: - Presenter
final class MainPresenter {

    weak var view: MainViewBehavior!
    var router: MainRouter!

    init(view: MainViewBehavior, router: MainRouter) {
        self.view = view
        self.router = router
    }
}

extension MainPresenter: MainEventHandler {

}
