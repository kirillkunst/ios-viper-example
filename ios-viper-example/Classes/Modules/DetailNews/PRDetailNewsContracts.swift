//
//  PRPRDetailNewsContracts.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit

// MARK: - Contracts
protocol DetailNewsViewBehavior: class {
    func setNewsItem(newsItem: NewsItem)
}

protocol DetailNewsEventHandler: ViewControllerEventHandler {
    func onModuleDidCreated(newsItemId: String)
}
