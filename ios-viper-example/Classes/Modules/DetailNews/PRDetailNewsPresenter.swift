//
//  PRDetailNewsPresenter.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit
import RxSwift

// MARK: - Presenter
final class DetailNewsPresenter {

    weak var view: DetailNewsViewBehavior!
    var router: DetailNewsRouter!
    private var newsItemId: String!
    private var newsService: NewsService!

    init(view: DetailNewsViewBehavior,
         router: DetailNewsRouter,
         newsService: NewsService) {
        self.view = view
        self.router = router
        self.newsService = newsService
    }
}

extension DetailNewsPresenter: DetailNewsEventHandler {

    func onViewDidLoad() {
        self.fetchNewsItem()
    }

    func onModuleDidCreated(newsItemId: String) {
        self.newsItemId = newsItemId
    }
    
}

extension DetailNewsPresenter {

    func fetchNewsItem() {
        let bag = DisposeBag()
        self.newsService.fetchNewsItem(id: self.newsItemId).subscribe(onNext: { [weak self] (newsItem) in
            if let item = newsItem {
                self?.view.setNewsItem(newsItem: item)
            }
        }, onError: { [weak self] (error) in
            self?.router.handle(error: error)
        }).disposed(by: bag)
    }
}
