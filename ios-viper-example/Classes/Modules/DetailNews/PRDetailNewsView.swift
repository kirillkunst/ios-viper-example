//
//  PRPRDetailNewsView.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit

// MARK: - View Controller
final class DetailNewsViewController: BaseViewController {
    
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    var handler: DetailNewsEventHandler!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.handler.onViewDidLoad()
    }

}

extension DetailNewsViewController: DetailNewsViewBehavior {

    func setNewsItem(newsItem: NewsItem) {
        self.titleLabel.text = newsItem.title
        self.detailLabel.text = newsItem.content
    }

}
