//
//  PRDetailNewsAssembly.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit
import DITranquillity

final class DetailNewsPart: DIPart {
    static func load(container: DIContainer) {
        container.register(DetailNewsRouter.init(view:errorHandler:))
            .lifetime(.objectGraph)

        container.register(DetailNewsPresenter.init(view:router:newsService:))
            .as(DetailNewsEventHandler.self)
            .lifetime(.objectGraph)

        container.register {
                UIStoryboard.init(name: "Main", bundle: nil)
                    .instantiateViewController(withIdentifier: "DetailNewsViewController") as! DetailNewsViewController
            }
            .as(DetailNewsViewBehavior.self)
            .injection(cycle: true, { $0.handler = $1 })
            .lifetime(.weakSingle)
    }
}

final class DetailNewsAssembly {
    class func createModule(newsItemId: String) -> DetailNewsViewController {
        let module: DetailNewsViewController = MainAppCoordinator.shared.container.resolve()
        module.handler.onModuleDidCreated(newsItemId: newsItemId)
        return module
    }
}
