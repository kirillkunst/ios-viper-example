//
//  PRNewsPresenter.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit
import RxSwift

// MARK: - Presenter
final class NewsPresenter {

    weak var view: NewsViewBehavior!
    var router: NewsRouter!

    private var newsService: NewsService!

    init(view: NewsViewBehavior,
         router: NewsRouter,
         newsService: NewsService) {
        self.view = view
        self.router = router
        self.newsService = newsService
    }
}

extension NewsPresenter: NewsEventHandler {

    func onViewDidLoad() {
        let bag = DisposeBag()
        self.newsService.fetchNews().subscribe(onNext: { (items) in
            self.view.set(items: items)
        }, onError: { [weak self] (error) in
            self?.router.handle(error: error)
        }).disposed(by: bag)
    }

    func openDetails(newsItem: NewsItem) {
        self.router.openNewsItem(newsItemId: newsItem.id)
    }
    
}
