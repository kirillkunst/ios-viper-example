//
//  NewsItemCell.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 06/02/2018.
//  Copyright © 2018 Paktor Ltd. All rights reserved.
//

import UIKit

class NewsItemCell: UICollectionViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelSubtitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(newsItem: NewsItem) {
        self.labelTitle.text = newsItem.title
        self.labelSubtitle.text = newsItem.content
    }

}
