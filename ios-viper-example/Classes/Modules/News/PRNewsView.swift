//
//  PRPRNewsView.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit
import IGListKit

// MARK: - View Controller
final class NewsViewController: BaseViewController {

    @IBOutlet var collectionView: UICollectionView!

    public lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self, workingRangeSize: 3)
    }()

    var handler: NewsEventHandler!
    var items: [ListDiffable] = []
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.handler.onViewDidLoad()
    }

    override func setup() {
        super.setup()
        self.navigationItem.title = "News"
        self.adapter.collectionView = self.collectionView
        self.adapter.dataSource = self
    }

    public func update(animated: Bool = false, completion: ListUpdaterCompletion? = nil) {
        adapter.performUpdates(animated: animated, completion: completion)
    }

}

// MARK: - IGListAdapterDataSource
extension NewsViewController: ListAdapterDataSource {

    public func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return items
    }

    public func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let adapter = NewsListAdapter()
        adapter.handleNewsItem = { [weak self] newItem in
            self?.handler.openDetails(newsItem: newItem)
        }
        return adapter
    }

    public func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }

}

extension NewsViewController: NewsViewBehavior {

    func set(items: [ListDiffable]) {
        self.items = items
        self.update(animated: true)
    }
}
