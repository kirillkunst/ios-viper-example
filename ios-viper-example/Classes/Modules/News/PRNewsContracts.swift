//
//  PRPRNewsContracts.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit
import IGListKit

// MARK: - Contracts
protocol NewsViewBehavior: class {
    func set(items: [ListDiffable])
}

protocol NewsEventHandler: ViewControllerEventHandler {
    func openDetails(newsItem: NewsItem)
}
