//
//  PRNewsRouter.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit

// MARK: - Router
final class NewsRouter: VIPERRouter<NewsViewController> {

    func openNewsItem(newsItemId: String) {
        let detailsNewsController = DetailNewsAssembly.createModule(newsItemId: newsItemId)

        if let navigationController = self.controller.navigationController {
            navigationController.pushViewController(detailsNewsController, animated: true)
        }
    }

}
