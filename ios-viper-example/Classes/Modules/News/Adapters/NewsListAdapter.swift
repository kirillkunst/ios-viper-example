//
//  NewsListAdapter.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 06/02/2018.
//  Copyright © 2018 Paktor Ltd. All rights reserved.
//

import Foundation
import IGListKit

public class NewsListAdapter: ListSectionController {

    var item: NewsItem?
    var handleNewsItem: ((NewsItem) -> ())?

    public override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: "NewsItemCell",
                                                          bundle: nil, for: self, at: index) as? NewsItemCell
        if let item = item {
            cell?.configure(newsItem: item)
        }

        return cell!
    }

    public override func didUpdate(to object: Any) {
        item = object as? NewsItem
    }

    public override func didSelectItem(at index: Int) {
        if let item = item {
            self.handleNewsItem?(item)
        }
    }

    override public func sizeForItem(at index: Int) -> CGSize {
        let widthCont = collectionContext?.containerSize.width
        let size = CGSize(width: widthCont!, height: 100)
        return size
    }
}
