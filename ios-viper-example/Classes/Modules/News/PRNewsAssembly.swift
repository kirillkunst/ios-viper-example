//
//  PRNewsAssembly.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 ios-viper-example. All rights reserved.
//

import UIKit
import DITranquillity

final class NewsPart: DIPart {
    static func load(container: DIContainer) {
        container.register(NewsRouter.init(view:errorHandler:))
            .lifetime(.objectGraph)

        container.register(NewsPresenter.init(view:router:newsService:))
            .as(NewsEventHandler.self)
            .lifetime(.objectGraph)

        container.register {
                UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
            }
            .as(NewsViewBehavior.self)
            .injection(cycle: true, { $0.handler = $1 })
            .lifetime(.weakSingle)
    }
}

final class NewsAssembly {
    class func createModule() -> NewsViewController {
        return MainAppCoordinator.shared.container.resolve()
    }
}
