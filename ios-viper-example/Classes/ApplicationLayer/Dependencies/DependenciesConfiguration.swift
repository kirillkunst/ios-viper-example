//
//  DependenciesConfiguration.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 5/25/18.
//  Copyright © 2018 Kirill Kunst. All rights reserved.
//

import UIKit
import DITranquillity

public protocol DependenciesConfiguration: class
{
    func setup()
    func setupModulesDependencies()
    func setupSharedDependencies()
    func configuredContainer() -> DIContainer
}

open class DependenciesConfigurationBase: DependenciesConfiguration
{
    public func configuredContainer() -> DIContainer
    {
        let container = DIContainer()
        container.append(framework: AppFramework.self)
        if !container.validate() {
            fatalError()
        }
        return container
    }

    // MARK: - Setup

    public func setup()
    {
        self.setupModulesDependencies()
        self.setupSharedDependencies()
    }

    public func setupModulesDependencies()
    {
        // Logger
        // Analytics
        // etc
    }

    public func setupSharedDependencies()
    {
        // Fabric for example
    }
}
