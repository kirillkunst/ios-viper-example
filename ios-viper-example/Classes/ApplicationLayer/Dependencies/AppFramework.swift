//
//  AppFramework.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 11/2/18.
//  Copyright © 2018 Kirill Kunst. All rights reserved.
//

import Foundation
import DITranquillity

public class AppFramework: DIFramework
{
    public static func load(container: DIContainer)
    {
        container.append(part: ServicesPart.self)
        container.append(part: ControllersPart.self)
        container.append(part: OtherPart.self)
    }
}

private class ServicesPart: DIPart
{
    static func load(container: DIContainer)
    {
        container.append(part: NewsServicePart.self)
    }
}

private class ControllersPart: DIPart
{
    static func load(container: DIContainer)
    {
        container.append(part: MainPart.self)
        container.append(part: NewsPart.self)
        container.append(part: DetailNewsPart.self)
    }
}

private class OtherPart: DIPart {
    static func load(container: DIContainer) {
        container.register(AlertErrorHandler.init)
            .as(ErrorHandling.self)
            .lifetime(.single)
    }
}
