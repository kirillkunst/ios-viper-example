//
//  AppDelegate.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 10/30/18.
//  Copyright © 2018 Kirill Kunst. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let dependencyConfiguration = DependenciesConfigurationBase()
        let coordinator = MainAppCoordinator(window: window, configuration: dependencyConfiguration)
        MainAppCoordinator.shared = coordinator
        coordinator.start()
        return true
    }
}
