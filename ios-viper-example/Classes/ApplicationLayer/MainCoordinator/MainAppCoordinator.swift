//
//  MainRouter.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/08/16.
//  Copyright © 2016 Kirill Kunst. All rights reserved.
//

import Foundation
import UIKit
import DITranquillity

open class MainAppCoordinator
{

    open static var shared: MainAppCoordinator!

    open var configuration: DependenciesConfiguration
    open var container: DIContainer
    public let router: AppRouter

    init(window: UIWindow, configuration: DependenciesConfiguration)
    {
        self.configuration = configuration
        self.configuration.setup()
        self.container = self.configuration.configuredContainer()
        self.router = AppRouter(window: window)
    }

    func start()
    {
        self.openMainModule()
    }

    // MARK: - Modules routing

    private func openMainModule()
    {
        self.router.openDefaultScene()
    }
}
