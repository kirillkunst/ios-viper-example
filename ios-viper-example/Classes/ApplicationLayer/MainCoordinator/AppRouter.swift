//
//  AppRouter.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 11/9/18.
//  Copyright © 2018 Kirill Kunst. All rights reserved.
//

import UIKit

public class AppRouter
{
    public var window: UIWindow

    init(window: UIWindow)
    {
        self.window = window
    }

    public func openDefaultScene()
    {
        let newsController = NewsAssembly.createModule()
        let navContainerController = UINavigationController(rootViewController: newsController)
        window.rootViewController = navContainerController
        window.makeKeyAndVisible()
    }
}
