//
//  BaseModel.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 28/03/2018.
//  Copyright © 2018 Paktor Ltd. All rights reserved.
//

import Foundation
import IGListKit

public class BaseModel: NSObject {
    public var id: String = ""
}

extension NSObject: ListDiffable {
    public func diffIdentifier() -> NSObjectProtocol {
        return self
    }

    public func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return isEqual(object)
    }
}
