//
//  NewsItem.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 Paktor Ltd. All rights reserved.
//

import Foundation

public class NewsItem: BaseModel
{
    public var title: String = ""
    public var content: String = ""

    init(id: String, title: String, content: String)
    {
        super.init()
        self.id = id
        self.title = title
        self.content = content
    }

}
