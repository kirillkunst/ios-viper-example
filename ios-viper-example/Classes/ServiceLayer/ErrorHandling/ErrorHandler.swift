//
//  ErrorHandler.swift
//  MRKit
//
//  Created by Kirill Kunst on 24/10/2016.
//  Copyright © 2016 Kirill Kunst. All rights reserved.
//

import Foundation
import UIKit

public protocol ErrorHandling
{
    func handleError(error: Error, viewController: UIViewController)
}
