//
//  AlertErrorHandler.swift
//  TotalCoin
//
//  Created by Kirill Kunst on 24/10/2016.
//  Copyright © 2016 Kirill Kunst. All rights reserved.
//

import Foundation
import UIKit

class AlertErrorHandler: ErrorHandling
{
    func handleError(error: Error, viewController: UIViewController)
    {
        var message = "\(error)"
        let nsError = error as NSError
        if let m = nsError.userInfo[NSLocalizedDescriptionKey] as? String {
            message = m
        }
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.show(viewController, sender: nil)
    }
}
