//
//  NewsService.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 02/02/2018.
//  Copyright © 2018 Paktor Ltd. All rights reserved.
//

import Foundation
import DITranquillity
import RxSwift

class NewsServicePart: DIPart
{
    static func load(container: DIContainer)
    {
        container.register{ NewsServiceImp() }
            .as(NewsService.self)
            .lifetime(.single)
    }
}

protocol NewsService
{
    func fetchNews() -> Observable<[NewsItem]>
    func fetchNewsItem(id: String) -> Observable<NewsItem?>
}

final class NewsServiceImp: NewsService
{
    private var news: [NewsItem] = []

    func fetchNews() -> Observable<[NewsItem]> {
        return Observable.create({ [unowned self] (observer) -> Disposable in
            if self.news.isEmpty {
                self.news = self.generateNews()
            }
            observer.onNext(self.news)
            return Disposables.create()
        })
    }

    func fetchNewsItem(id: String) -> Observable<NewsItem?> {
        return Observable.create({ [unowned self] (observer) -> Disposable in
            if self.news.isEmpty {
                self.news = self.generateNews()
            }
            let newsItem = self.news.filter({$0.id == id}).first
            observer.onNext(newsItem)
            return Disposables.create()
        })
    }


    private func generateNews() -> [NewsItem] {
        // return generated news here
        return [
            NewsItem(id: "1", title: "What’s Going to Happen to Ikea Founder’s Billions?", content: "When Ikea’s Ingvar Kamprad died Saturday at age 91, he was ranked No. 8 on the Bloomberg Billionaires Index thanks to his control of a global retail fortune valued at $58.7 billion. Ingvar Kamprad, Ikea’s Swedish Billionaire Founder, Dies at 91 His wealth will now be dissipated because of a unique structure put in place by Kamprad to secure the long-term independence and survival of the Ikea concept. Kamprad disputed his status as one of the richest men on the planet, having decades earlier placed control of the world’s largest furniture seller into a network of foundations and holding companies."),
            NewsItem(id: "2", title: "Preventing data leaks by stripping path information in HTTP Referrers", content: "To help prevent third party data leakage while browsing privately, Firefox Private Browsing Mode will remove path information from referrers sent to third parties starting in Firefox 59.Referrers can leak sensitive data Screenshot of healthcare.gov requests. Source: EFF An example of personal health data being sent to third parties from healthcare.gov. Source: EFF When you click a link in your browser to navigate to a new site, the new site you visit receives the exact address of the site you came from through the so-called “Referrer value”. For example, if you came to this Mozilla Security Blog from reddit.com, the browser would send blog.mozilla.org this: Referer: https://www.reddit.com/r/privacy/comments/Preventing_data_leaks_by_stripping_path_information_in_HTTP_Referrers/"),
            NewsItem(id: "3", title: "From Stanford University President to Chairman of Alphabet/Google", content: "When John Hennessy announced in 2015 that he was stepping down as president of Stanford University the following year, he said, \"The time has come to return to what brought me to Stanford—teaching and research.\" Turns out, you can’t keep a guy like Hennessy down on The Farm, at least in that small of a corral. Hennessy this week was named the new chairman of Alphabet, the parent company of Google. Eric Schmidt, who joined Google as CEO in 2001 to provide “adult supervision,” announced in December that he was stepping down. "),
            NewsItem(id: "4", title: "EBay Rises to Record High on Shift to Adyen; PayPal Tumbles", content: "EBay Inc. rose to a record high after giving an optimistic revenue forecast and unveiling plans to shift its payments business from long-time partner PayPal Holdings Inc. to Adyen BV, a global payments company based in the Netherlands. Shares of PayPal tumbled. PayPal is currently EBay’s payments processor, meaning merchants selling on the marketplace have to have PayPal accounts to accept funds, and it will remain a checkout option for EBay shoppers at least until July 2023. But Adyen will gradually take over processing EBay payments, beginning in North America this year and will handle a majority of transactions in 2021."),
            NewsItem(id: "5", title: "We Need Fewer Product Managers", content: "Note 1: This post may read like an attack on product management. That is not my intent at all. I love all things product. This post has been brewing as I interact with more and more companies struggling to define (and scale) product “management” in increasingly complex organizations. \n Note 2: Part of this is response to the refrain “there aren’t enough experienced product managers out there!” I disagree. There aren’t enough people who can magically fill the completely unreasonable and scattered roles being posted. There are, however, awesome people out there. Hire them to solve actual problems. \n We need fewer product managers. \n We need MORE internal startup co-founders, UX, customer advocates, domain experts, service designers, complexity untanglers, researchers, and reliable ways to interact directly with users/customers. We need more product thinking, and less product managing.")
        ]

    }
}
