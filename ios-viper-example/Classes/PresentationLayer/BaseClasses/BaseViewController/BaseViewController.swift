//
//  BaseViewController.swift
//  Kirill Kunst
//
//  Created by Kirill Kunst on 02/08/16.
//  Copyright © 2016 Kirill Kunst. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController
{

    let disposeBag = DisposeBag()
    var alertController: UIAlertController?

    deinit {
        print("[D] \(self) destroyed")
        NotificationCenter.default.removeObserver(self)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.initialize()
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.initialize()
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setup()
    }

    func initialize()
    {

    }

    func setup()
    {
    }

    func bindUI()
    {

    }

    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .default
    }

}

