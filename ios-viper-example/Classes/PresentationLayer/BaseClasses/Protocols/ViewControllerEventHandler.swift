//
//  ViewControllerEventHandler.swift
//  ios-viper-example
//
//  Created by Kirill Kunst on 06/02/2018.
//  Copyright © 2018 Paktor Ltd. All rights reserved.
//

import Foundation

public protocol ViewControllerEventHandler: class {
    func onViewDidLoad()
}

extension ViewControllerEventHandler {
    
}

