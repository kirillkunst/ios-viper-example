//
//  ViperRouter.swift
//  Kirill Kunst
//
//  Created by Kirill Kunst on 01/08/16.
//  Copyright © 2016 Kirill Kunst. All rights reserved.
//
import UIKit

public class VIPERRouter<T: UIViewController>
{
    weak var controller: T!
    var errorHandler: ErrorHandling

    init(view: T, errorHandler: ErrorHandling)
    {
        self.controller = view
        self.errorHandler = errorHandler
    }

    func handle(error: Error)
    {
        self.errorHandler.handleError(error: error, viewController: controller)
    }

}
