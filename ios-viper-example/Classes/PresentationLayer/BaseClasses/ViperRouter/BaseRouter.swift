//
//  Chaperone.swift
//  Router
//
//  Created by Kirill Kunst on 9/30/18.
//  Copyright © 2018 Kirill Kunst. All rights reserved.
//

import UIKit

public protocol BaseRouter
{
    func move()
}

public class PushRouter: BaseRouter
{
    let target: UIViewController
    let parent: UIViewController

    init(target: UIViewController, parent: UIViewController)
    {
        self.target = target
        self.parent = parent
    }

    public func move()
    {
        if let nc = parent.navigationController
        {
            self.present(target, using: nc)
        }
    }

    private func present(_ controller: UIViewController, using ncontroller: UINavigationController)
    {
        if ncontroller.topViewController != controller
        {
            ncontroller.pushViewController(controller, animated: true)
        }
    }
}

public final class ShowWindowRouter: BaseRouter
{
    let target: UIViewController
    let window: UIWindow

    init(target: UIViewController, window: UIWindow)
    {
        self.target = target
        self.window = window
    }

    public func move()
    {
         self.present(target, using: window)
    }

    private func present(_ controller: UIViewController, using window: UIWindow)
    {
        let windows = UIApplication.shared.windows.filter { $0 != window }
        for other in windows
        {
            other.isHidden = true
            other.rootViewController?.dismiss(animated: false, completion: {
            })
        }
        if let old = window.rootViewController
        {
            old.dismiss(animated: false, completion: {
                old.view.removeFromSuperview()
            })
        }
        window.rootViewController = controller
        window.makeKeyAndVisible()
        UIView.transition(with: window,
                          duration: 0.2,
                          options: .transitionCrossDissolve,
                          animations: nil,
                          completion: nil)
    }
}
